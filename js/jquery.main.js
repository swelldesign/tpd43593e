// page init
jQuery(function () {
	initFullPage();
	initBurgerOpen();
	initFormToggle();
	initGoToBlock();
	initInnerSlider();
	initInnerMenu();
	initSwitchContactPage();
	jQuery('input, textarea').placeholder();
});
/*! http://mths.be/placeholder v2.0.7 by @mathias */
;(function(window, document, $) {

	// Opera Mini v7 doesn’t support placeholder although its DOM seems to indicate so
	var isOperaMini = Object.prototype.toString.call(window.operamini) == '[object OperaMini]';
	var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini;
	var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini;
	var prototype = $.fn;
	var valHooks = $.valHooks;
	var propHooks = $.propHooks;
	var hooks;
	var placeholder;

	if (isInputSupported && isTextareaSupported) {

		placeholder = prototype.placeholder = function() {
			return this;
		};

		placeholder.input = placeholder.textarea = true;

	} else {

		placeholder = prototype.placeholder = function() {
			var $this = this;
			$this
				.filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
				.not('.placeholder')
				.bind({
					'focus.placeholder': clearPlaceholder,
					'blur.placeholder': setPlaceholder
				})
				.data('placeholder-enabled', true)
				.trigger('blur.placeholder');
			return $this;
		};

		placeholder.input = isInputSupported;
		placeholder.textarea = isTextareaSupported;

		hooks = {
			'get': function(element) {
				var $element = $(element);

				var $passwordInput = $element.data('placeholder-password');
				if ($passwordInput) {
					return $passwordInput[0].value;
				}

				return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
			},
			'set': function(element, value) {
				var $element = $(element);

				var $passwordInput = $element.data('placeholder-password');
				if ($passwordInput) {
					return $passwordInput[0].value = value;
				}

				if (!$element.data('placeholder-enabled')) {
					return element.value = value;
				}
				if (value == '') {
					element.value = value;
					// Issue #56: Setting the placeholder causes problems if the element continues to have focus.
					if (element != safeActiveElement()) {
						// We can't use `triggerHandler` here because of dummy text/password inputs :(
						setPlaceholder.call(element);
					}
				} else if ($element.hasClass('placeholder')) {
					clearPlaceholder.call(element, true, value) || (element.value = value);
				} else {
					element.value = value;
				}
				// `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
				return $element;
			}
		};

		if (!isInputSupported) {
			valHooks.input = hooks;
			propHooks.value = hooks;
		}
		if (!isTextareaSupported) {
			valHooks.textarea = hooks;
			propHooks.value = hooks;
		}

		$(function() {
			// Look for forms
			$(document).delegate('form', 'submit.placeholder', function() {
				// Clear the placeholder values so they don't get submitted
				var $inputs = $('.placeholder', this).each(clearPlaceholder);
				setTimeout(function() {
					$inputs.each(setPlaceholder);
				}, 10);
			});
		});

		// Clear placeholder values upon page reload
		$(window).bind('beforeunload.placeholder', function() {
			$('.placeholder').each(function() {
				this.value = '';
			});
		});

	}

	function args(elem) {
		// Return an object of element attributes
		var newAttrs = {};
		var rinlinejQuery = /^jQuery\d+$/;
		$.each(elem.attributes, function(i, attr) {
			if (attr.specified && !rinlinejQuery.test(attr.name)) {
				newAttrs[attr.name] = attr.value;
			}
		});
		return newAttrs;
	}

	function clearPlaceholder(event, value) {
		var input = this;
		var $input = $(input);
		if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
			if ($input.data('placeholder-password')) {
				$input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
				// If `clearPlaceholder` was called from `$.valHooks.input.set`
				if (event === true) {
					return $input[0].value = value;
				}
				$input.focus();
			} else {
				input.value = '';
				$input.removeClass('placeholder');
				input == safeActiveElement() && input.select();
			}
		}
	}

	function setPlaceholder() {
		var $replacement;
		var input = this;
		var $input = $(input);
		var id = this.id;
		if (input.value == '') {
			if (input.type == 'password') {
				if (!$input.data('placeholder-textinput')) {
					try {
						$replacement = $input.clone().attr({ 'type': 'text' });
					} catch(e) {
						$replacement = $('<input>').attr($.extend(args(this), { 'type': 'text' }));
					}
					$replacement
						.removeAttr('name')
						.data({
							'placeholder-password': $input,
							'placeholder-id': id
						})
						.bind('focus.placeholder', clearPlaceholder);
					$input
						.data({
							'placeholder-textinput': $replacement,
							'placeholder-id': id
						})
						.before($replacement);
				}
				$input = $input.removeAttr('id').hide().prev().attr('id', id).show();
				// Note: `$input[0] != input` now!
			}
			$input.addClass('placeholder');
			$input[0].value = $input.attr('placeholder');
		} else {
			$input.removeClass('placeholder');
		}
	}

	function safeActiveElement() {
		// Avoid IE9 `document.activeElement` of death
		// https://github.com/mathiasbynens/jquery-placeholder/pull/99
		try {
			return document.activeElement;
		} catch (err) {}
	}

}(this, document, jQuery));
//full page scroll
function initFullPage(){
	if($('#home-sections').length){
		var sliderPoints = $('.slider-points li');
		$('#home-sections').fullpage({
			scrollingSpeed: 1000,
			anchors:['home', 'key-projects', 'services', 'about-us', 'contact'],
			scrollOverflow: false,
			verticalCentered: false,
			css3: false,
			responsiveWidth: 800,
			onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){
				sliderPoints.removeClass('active').eq(nextSlideIndex).addClass('active');
			}
		});
		sliderPoints.on('click', function(){
			var activePoint = $(this);
			var moveTo = activePoint.index();
			sliderPoints.removeClass('active');
			activePoint.addClass('active');
			$.fn.fullpage.moveTo(1, moveTo);
		});
	}
}
//mobile menu control
function initBurgerOpen(){
	var menuOpener = $('.menu-switcher');
	//burger click open menu
	menuOpener.click(function(e){
		e.preventDefault();
		$('.nav-top').toggleClass('open');
	});
	//closing menu on section link click
	$('.nav-items a').on('click', function(){
		if($(this).parents('.nav-top').hasClass('open')){
			menuOpener.trigger('click');
		}
	});
	//removing mobile classes
	$(window).on('resize', function(){
		if($(window).width() > 768){
			$('.nav-top').removeClass('open');
		}
	});
}
//contact form toggle
function initFormToggle(){
	$('.toggle-form').on('click', function(e){
		e.preventDefault();
		$('.contact-form').slideToggle(200);
	});
}
//anchor slider on inner pages
function initGoToBlock(){
	var nav = $('.inner-nav-fixed .inner-nav a, .inner-mobile-nav a');
	var navDesk = $('.inner-nav-fixed .inner-nav a');
	var navMob = $('.inner-mobile-nav a');
	nav.on('click', function(e){
		e.preventDefault();
		var currentLink;
		if($(this).parent('li')){
			currentLink = $(this).parent('li').index();
		} else{
			currentLink = $(this).index();
		}
		var goTo = $(this).attr('href').split('#')[1];
		var additionalOffset = parseInt($('.content-section-inner.wide').css('padding-top').slice(0, -2)) +
			parseInt($('.wide h2').css('margin-top').slice(0, -2)) - 2;
		navDesk.removeClass('active');
		navMob.removeClass('active');
		navDesk.eq(currentLink).addClass('active');
		navMob.eq(currentLink).addClass('active');
		$('html, body').animate({
			scrollTop: $('#' + goTo).offset().top - additionalOffset
		}, 800);
	});
	$('.scroll-top').on('click', function(){
		$('html, body').animate({
			scrollTop: 0
		}, 800);
	});
}
//init inner slider
function initInnerSlider(){
	var sliderControl = $('.inner-slider-points');
	var sliderImages = $('.inner-page-images');
	$(window).on('resize', function(){
		if($(window).width() < 768){
			sliderControl.each(function(){
				var count = $(this).find('li');
				if(count.length > 1){
					count.eq(0).addClass('active');
				}
			});
			sliderImages.each(function(){
				var count = $(this).find('.image-holder');
				if(count.length > 1){
					count.eq(0).addClass('active');
				}
			});
			sliderControl.find('li').on('click', function(){
				var current = $(this);
				if(!current.hasClass('active')){
					var activeSlideNum = current.index();
					var activeSlides = current.parents('.inner-page-images').find('.image-holder');
					current.parent().find('li').removeClass('active');
					current.addClass('active');
					activeSlides.removeClass('active');
					activeSlides.eq(activeSlideNum).addClass('active');
				}
			});
		} else{
			sliderControl.each(function(){
				var count = $(this).find('li');
				count.removeClass('active');
			});
			sliderImages.each(function(){
				var count = $(this).find('.image-holder');
				count.removeClass('active');
			});
		}
	}).trigger('resize');
}
//init inner burger menu
function initInnerMenu(){
	var menuOpener = $('.inner-menu-switcher');
	//burger click open menu
	menuOpener.click(function(e){
		e.preventDefault();
		$('.content-section-inner.narrow').toggleClass('open');
	});
	//closing menu on section link click
	$('.inner-mobile-nav a').on('click', function(){
		if($(this).parents('.content-section-inner.narrow').hasClass('open')){
			menuOpener.trigger('click');
		}
	});
	//removing mobile classes
	$(window).on('resize', function(){
		if($(window).width() > 768){
			$('.content-section-inner.narrow').removeClass('open');
		}
	});
}
//switch divs
function initSwitchContactPage(){
	$(window).on('resize', function(){
		var switchMap = $('.contact-map-inner');
		var switchForm = $('.content-section-text');
		if($(window).width() < 768 && switchMap.prev().is(switchForm)){
			switchMap.each(function(i, el) {
				$(el).insertBefore($(el).prev());
			});
		}else if($(window).width() > 768 && switchMap.next().is(switchForm)){
			switchMap.each(function(i, el) {
				$(el).insertAfter($(el).next());
			});
		}
	}).trigger('resize');
}